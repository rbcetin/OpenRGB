#include "Detector.h"
#include "ZotacAmpereGPUController.h"
#include "RGBController.h"
#include "RGBController_ZotacAmpereGPU.h"
#include "i2c_smbus.h"
#include "pci_ids.h"

/******************************************************************************************\
*                                                                                          *
*   TestForZotacAmpereGPUController                                                        *
*                                                                                          *
*       Tests the given address to see if an RGB controller exists there.                  *
*                                                                                          *
\******************************************************************************************/

bool TestForZotacAmpereGPUController(i2c_smbus_interface* bus, u8 i2c_addr)
{
    /*---------------------------------------------------------*\
    | This command basically sets idle load profile.            |
    | Not really sure it's mandatory, but we can still use it   |
    | We can assume if the command succeeds, its valid device.  |
    \*---------------------------------------------------------*/
    u8 data_pkt[] = { ZOTAC_AMPERE_GPU_REG_LOAD_PROFILE, ZOTAC_GPU_IDLE_PROFILE };
    return (bus->i2c_write_block(i2c_addr, sizeof(data_pkt), data_pkt) >= 0);
}

/******************************************************************************************\
*                                                                                          *
*   DetectZotacAmpereGPUControllers                                                        *
*                                                                                          *
*       Detect ZOTAC Ampere RGB controllers on the enumerated I2C busses at address 0x49.  *
*                                                                                          *
*           bus - pointer to i2c_smbus_interface where RGB device is connected             *
*           dev - I2C address of RGB device                                                *
*                                                                                          *
\******************************************************************************************/

void DetectZotacAmpereGPUControllers(i2c_smbus_interface* bus, u8 i2c_addr, const std::string& name)
{
    if(TestForZotacAmpereGPUController(bus, i2c_addr))
    {
        ZotacAmpereGPUController*     controller     = new ZotacAmpereGPUController(bus, i2c_addr);
        RGBController_ZotacAmpereGPU* rgb_controller = new RGBController_ZotacAmpereGPU(controller);
        rgb_controller->name                         = name;

        ResourceManager::get()->RegisterRGBController(rgb_controller);
    }
}

REGISTER_I2C_PCI_DETECTOR(
    "ZOTAC GAMING GeForce RTX 4090 AMP EXTREME AIRO", 
    DetectZotacAmpereGPUControllers, NVIDIA_VEN, NVIDIA_RTX4090_DEV, GIGABYTE_SUB_VEN, GIGABYTE_RTX4090_GAMING_OC_24G_SUB_DEV, 0x49);
