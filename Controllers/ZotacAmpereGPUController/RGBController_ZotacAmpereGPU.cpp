/*-----------------------------------------*\
|  RGBController_ZotacAmpereGPU.cpp         |
|                                           |
|  Generic RGB Interface for OpenRGB        |
|  ZOTAC Ampere GPU Driver                  |
|                                           |
|  Rıdvan Berkay Çetin          1/11/2023   |
\*-----------------------------------------*/

#include "RGBController_ZotacAmpereGPU.h"

/**------------------------------------------------------------------*\
    @name ZOTAC Ampere GPU
    @category GPU
    @type I2C
    @save :white_check_mark:
    @direct :x:
    @effects :white_check_mark:
    @detectors DetectZotacAmpereGPUControllers
    @comment
\*-------------------------------------------------------------------*/

RGBController_ZotacAmpereGPU::RGBController_ZotacAmpereGPU(ZotacAmpereGPUController* controller_ptr)
{
    controller  = controller_ptr;

    name        = "ZOTAC GPU";
    vendor      = "ZOTAC";
    description = "ZOTAC Ampere-based RGB GPU Device";
    location    = controller->GetDeviceLocation();
    type        = DEVICE_TYPE_GPU;

    mode Static;
    Static.name           = "Static";
    Static.value          = ZOTAC_GPU_MODE_STATIC;
    Static.flags          = MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Static.color_mode     = MODE_COLORS_PER_LED;
    Static.brightness_min = ZOTAC_GPU_BRIGHTNESS_MIN;
    Static.brightness_max = ZOTAC_GPU_BRIGHTNESS_MAX;
    Static.brightness     = 0x64;

    modes.push_back(Static);

    mode Breathe;
    Breathe.name           = "Breathing";
    Breathe.value          = ZOTAC_GPU_MODE_BREATHE;
    Breathe.color_mode     = MODE_COLORS_PER_LED;
    Breathe.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Breathe.speed_min      = ZOTAC_GPU_SPEED_SLOWEST;
    Breathe.speed_max      = ZOTAC_GPU_SPEED_FASTEST;
    Breathe.brightness_min = ZOTAC_GPU_BRIGHTNESS_MIN;
    Breathe.brightness_max = ZOTAC_GPU_BRIGHTNESS_MAX;

    Breathe.speed          = 0x32;
    Breathe.brightness     = 0x64;

    modes.push_back(Breathe);

    mode Fade;
    Fade.name           = "Fade";
    Fade.value          = ZOTAC_GPU_MODE_FADE;
    Fade.color_mode     = MODE_COLORS_RANDOM;
    Fade.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Fade.speed_min      = ZOTAC_GPU_SPEED_SLOWEST;
    Fade.speed_max      = ZOTAC_GPU_SPEED_FASTEST;

    Fade.speed          = 0x32;

    modes.push_back(Fade);

    mode Wink;
    Wink.name           = "Wink";
    Wink.value          = ZOTAC_GPU_MODE_WINK;
    Wink.color_mode     = MODE_COLORS_PER_LED;
    Wink.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Wink.speed_min      = ZOTAC_GPU_SPEED_SLOWEST;
    Wink.speed_max      = ZOTAC_GPU_SPEED_FASTEST;
    Wink.brightness_min = ZOTAC_GPU_BRIGHTNESS_MIN;
    Wink.brightness_max = ZOTAC_GPU_BRIGHTNESS_MAX;

    Wink.speed          = 0x32;
    Wink.brightness     = 0x64;

    modes.push_back(Wink);

    mode Random;
    Random.name           = "Random";
    Random.value          = ZOTAC_GPU_MODE_RANDOM;
    Random.color_mode     = MODE_COLORS_PER_LED;
    Random.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Random.speed_min      = ZOTAC_GPU_SPEED_SLOWEST;
    Random.speed_max      = ZOTAC_GPU_SPEED_FASTEST;
    Random.brightness_min = ZOTAC_GPU_BRIGHTNESS_MIN;
    Random.brightness_max = ZOTAC_GPU_BRIGHTNESS_MAX;

    Random.speed          = 0x32;
    Random.brightness     = 0x64;

    modes.push_back(Random);

    mode Slide;
    Slide.name           = "Slide";
    Slide.value          = ZOTAC_GPU_MODE_SLIDE;
    Slide.color_mode     = MODE_COLORS_PER_LED;
    Slide.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_AUTOMATIC_SAVE | MODE_FLAG_HAS_DIRECTION_LR;
    Slide.speed_min      = ZOTAC_GPU_SPEED_SLOWEST;
    Slide.speed_max      = ZOTAC_GPU_SPEED_FASTEST;
    Slide.brightness_min = ZOTAC_GPU_BRIGHTNESS_MIN;
    Slide.brightness_max = ZOTAC_GPU_BRIGHTNESS_MAX;

    Slide.speed          = 0x32;
    Slide.brightness     = 0x64;
    Slide.direction      = MODE_DIRECTION_LEFT;

    modes.push_back(Slide);

    mode Rainbow;
    Rainbow.name           = "Rainbow";
    Rainbow.value          = ZOTAC_GPU_MODE_RAINBOW;
    Rainbow.color_mode     = MODE_COLORS_RANDOM;
    Rainbow.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE | MODE_FLAG_HAS_DIRECTION_LR;
    Rainbow.speed_min      = ZOTAC_GPU_SPEED_SLOWEST;
    Rainbow.speed_max      = ZOTAC_GPU_SPEED_FASTEST;

    Rainbow.speed          = 0x32;
    Rainbow.direction      = MODE_DIRECTION_LEFT;

    modes.push_back(Rainbow);

    mode Marquee;
    Marquee.name           = "Marquee";
    Marquee.value          = ZOTAC_GPU_MODE_MARQUEE;
    Marquee.color_mode     = MODE_COLORS_PER_LED;
    Marquee.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_AUTOMATIC_SAVE | MODE_FLAG_HAS_DIRECTION_LR;
    Marquee.speed_min      = ZOTAC_GPU_SPEED_SLOWEST;
    Marquee.speed_max      = ZOTAC_GPU_SPEED_FASTEST;
    Marquee.brightness_min = ZOTAC_GPU_BRIGHTNESS_MIN;
    Marquee.brightness_max = ZOTAC_GPU_BRIGHTNESS_MAX;

    Marquee.speed          = 0x32;
    Marquee.brightness     = 0x64;
    Marquee.direction      = MODE_DIRECTION_LEFT;

    modes.push_back(Marquee);

    mode Drip;
    Drip.name           = "Drip";
    Drip.value          = ZOTAC_GPU_MODE_DRIP;
    Drip.color_mode     = MODE_COLORS_PER_LED;
    Drip.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_AUTOMATIC_SAVE | MODE_FLAG_HAS_DIRECTION_LR;
    Drip.speed_min      = ZOTAC_GPU_SPEED_SLOWEST;
    Drip.speed_max      = ZOTAC_GPU_SPEED_FASTEST;
    Drip.brightness_min = ZOTAC_GPU_BRIGHTNESS_MIN;
    Drip.brightness_max = ZOTAC_GPU_BRIGHTNESS_MAX;

    Drip.speed          = 0x32;
    Drip.brightness     = 0x64;
    Drip.direction      = MODE_DIRECTION_LEFT;

    modes.push_back(Drip);

    mode Path;
    Path.name           = "Path";
    Path.value          = ZOTAC_GPU_MODE_PATH;
    Path.color_mode     = MODE_COLORS_RANDOM;
    Path.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE | MODE_FLAG_HAS_DIRECTION_LR;
    Path.speed_min      = ZOTAC_GPU_SPEED_SLOWEST;
    Path.speed_max      = ZOTAC_GPU_SPEED_FASTEST;
    Path.brightness_min = ZOTAC_GPU_BRIGHTNESS_MIN;
    Path.brightness_max = ZOTAC_GPU_BRIGHTNESS_MAX;

    Path.speed          = 0x32;
    Path.brightness     = 0x64;
    Path.direction      = MODE_DIRECTION_LEFT;

    modes.push_back(Path);

    SetupZones();
}

RGBController_ZotacAmpereGPU::~RGBController_ZotacAmpereGPU()
{
    delete controller;
}

void RGBController_ZotacAmpereGPU::SetupZones()
{
    /*---------------------------------------------------------*\
    | This device only multiple zones but no support atm.       |
    \*---------------------------------------------------------*/
    zone* new_zone          = new zone();
    led*  new_led           = new led();

    new_zone->name          = "All Zones";
    new_zone->type          = ZONE_TYPE_SINGLE;
    new_zone->leds_min      = 1;
    new_zone->leds_max      = 1;
    new_zone->leds_count    = 1;
    new_zone->matrix_map    = NULL;

    new_led->name           = "GPU LED";

    /*---------------------------------------------------------*\
    | Push the zone and LED on to device vectors                |
    \*---------------------------------------------------------*/
    leds.push_back(*new_led);
    zones.push_back(*new_zone);

    SetupColors();
    SetupInitialValues();
}

void RGBController_ZotacAmpereGPU::SetupInitialValues()
{
    /*---------------------------------------------------------*\
    | Retrieve current values by reading the device             |
    \*---------------------------------------------------------*/
    unsigned int speed, brightness, direction;

    controller->GetMode(colors[0], active_mode, speed, brightness, direction);
    modes[active_mode].speed = speed;
    modes[active_mode].brightness = brightness;
    modes[active_mode].direction = direction;

    SignalUpdate();
}

void RGBController_ZotacAmpereGPU::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

void RGBController_ZotacAmpereGPU::DeviceUpdateLEDs()
{
    DeviceUpdateMode();
}

void RGBController_ZotacAmpereGPU::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateMode();
}

void RGBController_ZotacAmpereGPU::UpdateSingleLED(int /*led*/)
{
    DeviceUpdateMode();
}

void RGBController_ZotacAmpereGPU::DeviceUpdateMode()
{
    controller->SetMode(colors[0], modes[active_mode].value, modes[active_mode].speed, modes[active_mode].brightness, modes[active_mode].direction);
}
