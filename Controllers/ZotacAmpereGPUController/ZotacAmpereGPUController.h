/*-----------------------------------------*\
|  ZotacAmpereGPUController.cpp             |
|                                           |
|  Definitions and types for ZOTAC GeForce  |
|  RTX 40 series GPU lighting controller    |
|                                           |
|  Rıdvan Berkay Çetin          1/11/2023   |
\*-----------------------------------------*/

#include <string>
#include "i2c_smbus.h"
#include "RGBController.h"

#pragma once

/*---------------------------------------------------------*\
| Zotac Ampere REGS                                         |
\*---------------------------------------------------------*/
enum
{
    ZOTAC_AMPERE_GPU_REG_COLOR_AND_MODE = 0xA0,
    ZOTAC_AMPERE_GPU_REG_LOAD_PROFILE   = 0xA2,
};

/*---------------------------------------------------------*\
| Zotac Ampere RGB Modes                                    |
\*---------------------------------------------------------*/
enum
{
    ZOTAC_GPU_MODE_STATIC                = 0x00,
    ZOTAC_GPU_MODE_BREATHE               = 0x01,
    ZOTAC_GPU_MODE_FADE                  = 0x02,
    ZOTAC_GPU_MODE_WINK                  = 0x03,
    ZOTAC_GPU_MODE_FLASH                 = 0x04,
    ZOTAC_GPU_MODE_SHINE                 = 0x05,
    ZOTAC_GPU_MODE_RANDOM                = 0x06,
    ZOTAC_GPU_MODE_SLIDE                 = 0x07,
    ZOTAC_GPU_MODE_RAINBOW               = 0x08,
    ZOTAC_GPU_MODE_MARQUEE               = 0x09,
    ZOTAC_GPU_MODE_DRIP                  = 0x0A,
    ZOTAC_GPU_MODE_DANCE                 = 0x0B,
    ZOTAC_GPU_MODE_DUET                  = 0x17,
    ZOTAC_GPU_MODE_PATH                  = 0x18,
};

/*---------------------------------------------------------*\
| Zotac Ampere RGB CIRCUIT                                |
\*---------------------------------------------------------*/
enum
{
    ZOTAC_GPU_CIRCUIT_ON              = 0x00,
    ZOTAC_GPU_CIRCUIT_OFF             = 0x01,
};

/*---------------------------------------------------------*\
| Zotac Ampere RGB ZONES                                    |
\*---------------------------------------------------------*/
enum
{
    ZOTAC_GPU_BIG_STRIP                     = 0x00,
    ZOTAC_GPU_FAN_BOTTOM_TOP_STRIP          = 0x01,
    ZOTAC_GPU_FAN_BOTTOM_THIN_STRIP         = 0x02,
    ZOTAC_GPU_RIGHT_CIRCULAR_STRIP          = 0x03,
    ZOTAC_GPU_BACKPLATE_LOGO                = 0x04,
    ZOTAC_GPU_PCB_LINE_STRIP                = 0x05,
    ZOTAC_GPU_ALL_ZONES_WITH_EXTERNAL_STRIP = 0x06,
    ZOTAC_GPU_ALL_ZONES                     = 0x07,
    ZOTAC_GPU_ONLY_EXTERNAL_STRIP           = 0x08,
};

/*---------------------------------------------------------*\
| Zotac Ampere Speed & Brightness Limits                    |
\*---------------------------------------------------------*/
enum
{
    ZOTAC_GPU_SPEED_SLOWEST = 0x00,
    ZOTAC_GPU_SPEED_FASTEST = 0x64,
    ZOTAC_GPU_BRIGHTNESS_MAX = 0x64,
    ZOTAC_GPU_BRIGHTNESS_MIN = 0x00, 
};


/*---------------------------------------------------------*\
| Zotac Ampere MISC                                         |
\*---------------------------------------------------------*/
enum
{
    ZOTAC_ENABLED_EXTERNAL_LED_STRIP  = 0x01,
    ZOTAC_DISABLED_EXTERNAL_LED_STRIP = 0x00,
    ZOTAC_SYNC_ALL_LEDS               = 0x02, // ??
    ZOTAC_DONT_SYNC_ALL_LEDS          = 0x01, // ??
    ZOTAC_GPU_IDLE_PROFILE            = 0x00,
    ZOTAC_GPU_LOAD_PROFILE            = 0x01,
    ZOTAC_GPU_LEDS_ON                 = 0x01,
    ZOTAC_GPU_LEDS_OFF                = 0x00,

};

class ZotacAmpereGPUController
{
public:
    ZotacAmpereGPUController(i2c_smbus_interface* bus, u8 dev);
    ~ZotacAmpereGPUController();

    std::string GetDeviceLocation();

    void GetMode(RGBColor& color, int& mode, unsigned int& speed, unsigned int& brightness, unsigned int& direction);
    void SetMode(RGBColor color, int mode, unsigned int speed, unsigned int brightness, unsigned int direction);

private:
    i2c_smbus_interface* bus;
    u8                   dev;
};
