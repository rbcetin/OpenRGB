/*-----------------------------------------*\
|  ZotacAmpereGPUController.cpp             |
|                                           |
|  Driver for ZOTAC GeForce RTX 40 series   |
|  GPU lighting controller                  |
|                                           |
|  Rıdvan Berkay Çetin          1/11/2023   |
\*-----------------------------------------*/

#include "ZotacAmpereGPUController.h"
#include "LogManager.h"

ZotacAmpereGPUController::ZotacAmpereGPUController(i2c_smbus_interface* bus, u8 dev)
{
    this->bus = bus;
    this->dev = dev;
}

ZotacAmpereGPUController::~ZotacAmpereGPUController()
{
}

std::string ZotacAmpereGPUController::GetDeviceLocation()
{
    std::string return_string(bus->device_name);
    char addr[5];
    snprintf(addr, 5, "0x%02X", dev);
    return_string.append(", address ");
    return_string.append(addr);
    return("I2C: " + return_string);
}

void ZotacAmpereGPUController::GetMode(RGBColor& color, int& mode, unsigned int& speed, unsigned int& brightness, unsigned int& direction)
{
    /*-----------------------------------------*\
    | Could not find a way to retrieve the mode |
    \*-----------------------------------------*/
    mode  = ZOTAC_GPU_MODE_STATIC;
    color = ToRGBColor(0,0,0);
    speed = ZOTAC_GPU_SPEED_SLOWEST;
    brightness = ZOTAC_GPU_BRIGHTNESS_MAX;
    direction = MODE_DIRECTION_LEFT;
}

void ZotacAmpereGPUController::SetMode(RGBColor color, int mode, unsigned int speed, unsigned int brightness, unsigned int direction)
{
    u8 data_pkt[] =
    {
        ZOTAC_AMPERE_GPU_REG_COLOR_AND_MODE,
        ZOTAC_GPU_LEDS_ON,
        0x00, // 1
        0x00, // 2
        0x00, // 3
        ZOTAC_GPU_IDLE_PROFILE,
        ZOTAC_GPU_ALL_ZONES,
        (u8)mode,
        (u8)RGBGetRValue(color),
        (u8)RGBGetGValue(color),
        (u8)RGBGetBValue(color),
        (u8)speed,
        (u8)brightness,
        (u8)direction,
        0x08, // 13
        0x01, // sync all leds test
        0x00, // circuit status
        (u8)RGBGetRValue(color), // r2
        (u8)RGBGetGValue(color), //  g2
        (u8)RGBGetBValue(color), // b3
        0x00, // Will not add external strip support, but can be added in the future.
        0x00, // 20
        0x00, // 21
        0x00, // 22
        0x00, // 23
        0x00, // 24
        0x00, // 25
        0x00, // 26
        0x00, // 27
        0x00, // 28
        0x00  // 29
    };

    bus->i2c_write_block(dev, sizeof(data_pkt), data_pkt);
}
