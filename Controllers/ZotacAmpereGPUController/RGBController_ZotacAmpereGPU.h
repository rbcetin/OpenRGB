/*-----------------------------------------*\
|  RGBController_ZotacAmpereGPU.h           |
|                                           |
|  Generic RGB Interface for OpenRGB        |
|  ZOTAC Ampere GPU Driver                  |
|                                           |
|  Rıdvan Berkay Çetin          1/11/2023   |
\*-----------------------------------------*/

#pragma once

#include "RGBController.h"
#include "ZotacAmpereGPUController.h"

class RGBController_ZotacAmpereGPU : public RGBController
{
public:
    RGBController_ZotacAmpereGPU(ZotacAmpereGPUController* controller_ptr);
    ~RGBController_ZotacAmpereGPU();

    void SetupInitialValues();
    void SetupZones();

    void ResizeZone(int zone, int new_size);

    void DeviceUpdateLEDs();
    void UpdateZoneLEDs(int zone);
    void UpdateSingleLED(int led);

    void DeviceUpdateMode();

private:
    ZotacAmpereGPUController* controller;
};
